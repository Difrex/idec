#!/usr/bin/env python3

import os
import api
import base64
import points
from api import base
from api import filter
from api import freq
from api import fecho
from api.bottle import response, request, route, post, static_file, run


@route("/list.txt")
def list_scheme():
    response.set_header("Content-Type", "text/plain; charset=utf-8")
    api.load_config()
    ret = ""
    for echoarea in api.echoareas:
        ret += "{0}:{1}:{2}".format(
            echoarea["name"], base.echoarea_count(echoarea["name"]), echoarea["description"]
        )
    return ret


@route("/blacklist.txt")
def blacklist_scneme():
    response.set_header("Content-Type", "text/plain; charset=utf-8")
    return "\n".join(base.read_blacklist())


@route("/e/<echoarea>")
def e_scheme(echoarea):
    response.set_header("Content-Type", "text/plain; charset=utf-8")
    if filter.is_echoarea(echoarea):
        return "\n".join(api.clean_echoarea(echoarea))
    else:
        return f"error: incorrect echoarea ({echoarea})"


@route("/m/<msgid>")
def m_scheme(msgid):
    response.set_header("Content-Type", "text/plain; charset=utf-8")
    if filter.is_msgid(msgid):
        return base.read_message(msgid)
    else:
        return f"error: incorrect msgid ({msgid})"


@route("/u/e/<echoareas:path>")
def ue_scheme(echoareas):
    response.set_header("Content-Type", "text/plain; charset=utf-8")
    echoareas = echoareas.split("/")
    start, end, r = 0, 0, False
    if ":" in echoareas[-1]:
        params = echoareas[-1].split(":")
        start, end, r = int(params[0]), int(params[1]), True
        echoareas = echoareas[:-1]
    ret = ""
    for echoarea in echoareas:
        if filter.is_echoarea(echoarea):
            ss = start
            if start < 0 and start + int(base.echoarea_count(echoarea)) < 0:
                ss = 0
            elif start > int(base.echoarea_count(echoarea)):
                ss = end * -1
            msgids = (
                api.clean_echoarea(echoarea)[ss:]
                if start + end == 0
                else api.clean_echoarea(echoarea)[ss: ss + end]
                if r
                else api.clean_echoarea(echoarea)
            )
            ret += echoarea + "\n"
            if msgids:
                ret += "\n".join(msgids) + "\n"
    return ret


@route("/u/m/<msgids:path>")
def um_scheme(msgids):
    response.set_header("Content-Type", "text/plain; charset=utf-8")
    msgids = msgids.split("/")
    ret = ""
    for msgid in msgids:
        if filter.is_msgid(msgid):
            encoded = base64.b64encode(base.read_message(msgid).encode()).decode("utf-8")
            ret += f"{msgid}:{encoded}\n"
    return ret


@route("/u/point/<pauth>/<tmsg>")
def upoint_scheme(pauth, tmsg):
    response.set_header("Content-Type", "text/plain; charset=utf-8")
    point, name = points.is_point(pauth)
    if point:
        return base.toss_message(point, name, tmsg)
    return "error: auth error"


@post("/u/point")
def upoint_post_scheme():
    response.set_header("Content-Type", "text/plain; charset=utf-8")
    point, name = points.is_point(request.POST["pauth"])
    if point:
        return base.toss_message(point, name, request.POST["tmsg"])
    else:
        return "error: auth error"


@post("/u/push")
def upush_scheme():
    response.set_header("Content-Type", "text/plain; charset=utf-8")
    nauth = request.POST["nauth"]
    if points.is_pusher(nauth):
        bundle = request.POST["upush"]
        echoarea = request.POST["echoarea"]
        return base.debundle(echoarea, bundle)
    else:
        return "error: auth error"


@route("/x/c/<echoareas:path>")
def xc_scheme(echoareas=[]):
    response.set_header("Content-Type", "text/plain; charset=utf-8")
    echoareas = echoareas.split("/")
    ret = ""
    for echoarea in echoareas:
        if filter.is_echoarea(echoarea):
            ret += f"{echoarea}:{base.echoarea_count(echoarea)}\n"
    return ret


@route("/x/filelist")
@route("/x/filelist/<pauth>")
def xfilelist_scheme(pauth=False):
    response.set_header("Content-Type", "text/plain; charset=utf-8")
    return freq.get_filelist(pauth)


@post("/x/file")
@route("/x/file/<filename:path>")
def xfile_scheme(filename=False):
    pauth = False
    if request.method == "POST":
        filename = request.POST["filename"]
        pauth = request.POST["pauth"]
    if freq.file_exist(pauth, filename):
        print(1)
        return static_file(filename, "files/")


@route("/f/c/<fechoareas:path>")
def fc_scheme(fechoareas):
    response.set_header("Content-Type", "text/plain; charset=utf-8")
    fechoareas = fechoareas.split("/")
    ret = ""
    for fechoarea in fechoareas:
        if filter.is_fechoarea(fechoarea):
            ret += f"{fechoarea}:{fecho.fechoarea_count(fechoarea)}\n"
    return ret


@route("/f/list.txt")
def flist_scheme():
    response.set_header("Content-Type", "text/plain; charset=utf-8")
    api.load_config()
    ret = ""
    print(api.fechoareas)
    for fechoarea in api.fechoareas:
        ret += "{0}:{1}:{2}".format(
            fechoarea["name"], fecho.fechoarea_count(fechoarea["name"]), fechoarea["description"]
        )
    return ret


@route("/f/blacklist.txt")
def fblacklist_scneme():
    response.set_header("Content-Type", "text/plain; charset=utf-8")
    return "\n".join(fecho.read_blacklist())


@route("/f/e/<fechoareas:path>")
def fe_scheme(fechoareas):
    response.set_header("Content-Type", "text/plain; charset=utf-8")
    fechoareas = fechoareas.split("/")
    start, end, r = 0, 0, False
    if ":" in fechoareas[-1]:
        params = fechoareas[-1].split(":")
        start, end, r = int(params[0]), int(params[1]), True
        fechoareas = fechoareas[:-1]
    ret = ""
    for fechoarea in fechoareas:
        if filter.is_fechoarea(fechoarea):
            ss = start
            if start < 0 and start + int(fecho.fechoarea_count(fechoarea)) < 0:
                ss = 0
            elif start > int(fecho.fechoarea_count(fechoarea)):
                ss = end * -1
            msgids = (
                api.clean_fechoarea(fechoarea)[ss:]
                if start + end == 0
                else api.clean_fechoarea(fechoarea)[ss: ss + end]
                if r
                else api.clean_fechoarea(fechoarea)
            )
            ret += fechoarea + "\n"
            if msgids:
                ret += "\n".join(msgids) + "\n"
    return ret


@route("/f/f/<fechoarea>/<fid>")
def ff_scheme(fechoarea, fid):
    if not os.path.exists(f"fecho/{fechoarea}"):
        return f"file echoarea {fechoarea} not found"
    blacklist = fecho.read_blacklist()
    if fid in blacklist:
        return "file blocked"
    _fecho = open(f"fecho/{fechoarea}", "r").read().split("\n")
    fids = [line.split(":")[0] for line in _fecho]
    files = [line.split(":")[1] for line in _fecho if len(_fecho) > 0]
    if not fid in fids:
        return "file not found"
    return static_file(files[fids.index(fid)], "files/{fecho}/")


@post("/f/p")
def fp_scheme():
    response.set_header("Content-Type", "text/plain; charset=utf-8")
    pauth = request.POST["pauth"] or False
    fecho = request.POST["fecho"] or False
    dsc = request.POST["dsc"] or False
    f = request.files.get("file") or False
    if pauth and fecho and dsc and f:
        if not filter.is_filename(f.raw_filename):
            return "error: incorrect filename"
        if not filter.is_fechoarea(fecho):
            return "error: incorrect fileecho area"
        point, name = base.is_point(pauth)
        if point:
            f.save("temp")
            if not os.path.exists(f"files/{fecho}"):
                os.makedirs(f"files/{fecho}")


if __name__ == "__main__":
    print(open("logo.txt").read())
    base.check_base()
    api.load_config()
    run(host="0.0.0.0", port=62220)
