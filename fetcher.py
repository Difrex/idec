#!/usr/bin/env python3

import base64, sys, urllib.request
from api import base
from api import filter
from api import fecho

config_file = "fetcher.cfg"

def load_config(config_file):
    global node, echoareas, fechoareas, depth, fdepth
    node = ""
    echoareas = []
    fechoareas = []
    depth = 0
    fdepth = 0
    try:
        f = open(config_file).read().split("\n")
        for line in f:
            param = line.split()
            if len(param) > 1:
                if param[0] == "node":
                    node = param[1]
                elif param[0] == "echo":
                    echoareas.append(param[1])
                elif param[0] == "fecho":
                    fechoareas.append(param[1])
                elif param[0] == "depth":
                    depth = param[1]
                elif param[0] == "fdepth":
                    fdepth = param[1]
    except:
        sys.exit(1)

def split(l, size=40):
    for i in range(0, len(l), size):
        yield l[i:i+size]

def download_index():
    print("download echoareas index")
    ids = []
    if depth == 0:
        r = urllib.request.Request("%su/e/%s" % (node, "/".join(echoareas)))
    else:
        r = urllib.request.Request("%su/e/%s/-%s:%s" % (node, "/".join(echoareas), depth, depth))
    with urllib.request.urlopen(r) as f:
        raw = f.read().decode("utf-8").split()
        ids = [msgid for msgid in raw if filter.is_msgid(msgid)]
    return ids

def read_local_index():
    for echoarea in echoareas:
        for msgid in base.read_echoarea(echoarea):
            yield msgid

def build_diff(local, remote):
    return [i for i in remote if i not in local]

def download_bundle(msgids):
    bundle = []
    print("fetch: %su/m/%s" % (node, "/".join(msgids)))
    r = urllib.request.Request("%su/m/%s" % (node, "/".join(msgids)))
    with urllib.request.urlopen(r) as f:
        bundle = f.read().decode("utf-8").split()
    return bundle

def debundle(bundle):
    for line in bundle:
        row = line.split(":")
        message = base64.urlsafe_b64decode(row[1]).decode("utf-8")
        echoarea = message.split("\n")[1]
        base.save_message(echoarea, row[0], message)

def download_mail():
    index = build_diff(list(read_local_index()), download_index())
    if len(index) == 0:
        print("new messages not found")
    for s in split(index):
        debundle(download_bundle(s))

def read_local_fileindex():
    for fechoarea in fechoareas:
        for f in fecho.read_fechoarea(fechoarea):
            yield f

def download_fecho_index():
    print("download file echoareas index")
    ids = []
    if fdepth == 0:
        r = urllib.request.Request("%sf/e/%s" % (node, "/".join(fechoareas)))
    else:
        r = urllib.request.Request("%sf/e/%s/-%s:%s" % (node, "/".join(fechoareas), fdepth, fdepth))
    with urllib.request.urlopen(r) as f:
        raw = f.read().decode("utf-8").split("\n")
        fechoarea = ""
        for line in raw:
            if not ":" in line:
                fechoarea = line
            else:
                if len(line) > 0:
                    ids.append([fechoarea, line])
    return ids

def build_fileecho_diff(local, remote):
    return [i for i in remote if i not in local]

def download_file(fechoarea, fid):
    frow = fid.split(":")
    print("download: %sf/f/%s/%s %s" %(node, fechoarea, frow[0], frow[1]))
    r = urllib.request.Request("%sf/f/%s/%s" % (node, fechoarea, frow[0]))
    out = urllib.request.urlopen(r)
    fecho.save_file(fechoarea, frow, out)

def download_filemail():
    index = build_fileecho_diff(list(read_local_fileindex()), download_fecho_index())
    if len(index) == 0:
        print("new files not found")
    for s in index:
        download_file(s[0], s[1])

args = sys.argv
if len(args) > 1:
    config_file = args[1]

base.check_base()
load_config(config_file)
if len(echoareas) > 0:
    download_mail()
else:
    print("no echoareas in subscription")
if len(fechoareas) > 0:
    download_filemail()
else:
    print("no file echoareas in subsription")
