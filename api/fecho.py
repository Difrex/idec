import os

def read_blacklist():
    return open("fblacklist.txt").read().split()

def fechoarea_count(fechoarea):
    try:
        return len(open("fecho/%s" % fechoarea, "r").read().strip().split("\n"))
    except:
        return 0

def read_fechoarea(fechoarea):
    try:
        for line in open("fecho/%s" % fechoarea).read().strip().split("\n"):
            yield [fechoarea, line]
    except:
        return []

def save_file(fecho, frow, out):
    file_size = 0
    block_size = 8192
    if not os.path.exists("files/%s" % fecho):
        os.mkdir("files/%s" % fecho)
    f = open("files/%s/%s" % (fecho, frow[1]), "wb")
    while True:
        buffer = out.read(block_size)
        if not buffer:
            break
        file_size += len(buffer)
        f.write(buffer)
    f.close()
    open("fecho/%s" % fecho, "a").write(":".join(frow) + "\n")
    open("files/indexes/files.txt", "a").write("%s:%s:%s\n" % (fecho, frow[1], ":".join(frow[4:])))
