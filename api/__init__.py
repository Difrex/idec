from api import base

node = ""
description = ""
echoareas = []
fechoareas = []

def load_config():
    global node, description, echoareas, fechoareas
    node = ""
    description = ""
    echoareas = []
    fechoareas = []
    config = open("idec.cfg").read().split("\n")
    for line in config:
        param = line.split()
        if len(param) > 0:
            if param[0] == "node":
                node = param[1]
            elif param[0] == "desc":
                description = " ".join(param[1:])
            elif param[0] == "echo":
                echoareas.append({"name": param[1], "description": " ".join(param[2:])})
            elif param[0] == "fecho":
                fechoareas.append({"name": param[1], "description": " ".join(param[2:])})

def clean_echoarea(echoarea):
    blacklist = set(base.read_blacklist())
    return [msgid for msgid in base.read_echoarea(echoarea) if not msgid in blacklist]

def clean_fechoarea(fechoarea):
    blacklist = set(fecho.read_blacklist())
    return [msgid for msgid in fecho.read_fechoarea(fechoarea) if not msgid in blacklist]
