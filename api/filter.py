import re

def is_msgid(msgid):
    return re.match("[0-9a-zA-Z]{20}", msgid)

def is_echoarea(echoarea):
    return re.match("[0-9a-z_\-\.]{1,60}\.[0-9a-z_\-\.]{1,60}", echoarea)

def is_fechoarea(fechoarea):
    return re.match("[0-9a-z_\-\.]{3,120}", fechoarea)

def is_filename(filename):
    return re.match("[0-9a-zA-Z_\-\.]{5,120}", filename)
