import os
from api import base

def file_exist(pauth, filename):
    point, name = base.is_point(pauth)
    index = open("files/indexes/public_files.txt").read().split("\n")
    if point:
        index += open("files/indexes/files.txt").read().split("\n")
        if os.path.exists("files/indexes/%s_files.txt" % name):
            index += open("files/indexes/%s_files.txt" % name).read().split("\n")
    for f in index:
        if f.startswith(filename):
            return True
    return False

def get_filelist(pauth):
    ret = open("files/indexes/public_files.txt").read().split("\n")
    point, name = base.is_point(pauth)
    if point:
        ret += open("files/indexes/files.txt").read().split("\n")
        if os.path.exists("files/indexes/%s_files.txt" % name):
            ret += open("files/indexes/%s_files.txt" % name).read().split("\n")
    ret.sort()
    index = []
    for line in ret:
        f = line.split(":")
        if len(f) == 2:
            index.append("%s:%s:%s" % (f[0], os.stat("files/%s" % f[0]).st_size, f[1]))
    return "\n".join(index)
