import api, hashlib, os, base64, time, sys
from api import filter

def check_file(filename):
    if not os.path.exists(filename):
        open(filename, "w")

def check_dir(dirname):
    if not os.path.exists(dirname):
        os.makedirs(dirname)

def check_base():
    check_file("points.txt")
    check_file("pushers.txt")
    check_file("blacklist.txt")
    check_file("fblacklist.txt")
    check_dir("echo")
    check_dir("msg")
    check_dir("fecho")
    check_dir("files")
    check_dir("files/indexes")
    check_file("files/indexes/files.txt")
    check_file("files/indexes/public_files.txt")
    if not os.path.exists("idec.cfg"):
        open("idec.cfg", "w").write(open("idec.def.cfg").read())

def read_blacklist():
    return open("blacklist.txt").read().split()

def echoarea_count(echoarea):
    return len(read_echoarea(echoarea))

def read_echoarea(echoarea):
    try:
        return open("echo/%s" % echoarea).read().split()
    except:
        return []

def read_message(msgid):
    try:
        return open("msg/%s" % msgid).read()
    except:
        return None

def save_message(echoarea, msgid, message):
    open("echo/%s" % echoarea, "a").write("%s\n" % (msgid))
    open("msg/%s" % msgid, "w").write(message)

def hsh(msg):
    return base64.urlsafe_b64encode(hashlib.sha256(msg.encode()).digest()).decode("utf-8").replace("-", "A").replace("_", "z")[:20]

def toss_message(point, msgfrom, tmsg):
    raw = base64.b64decode(tmsg.encode()).decode("utf-8").split("\n")
    echoarea = raw[0]
    if not filter.is_echoarea(echoarea):
        return "error: incorrect echoarea (%s)" % echoarea
    msgto = raw[1]
    subject = raw[2]
    addr = "%s,%s" % (api.node, point)
    if raw[4].startswith("@Repto:"):
        tags = "ii/ok/repto/%s" % raw[4].replace("@Repto:", "")
        body = "\n".join(raw[5:])
    else:
        tags = "ii/ok"
        body = "\n".join(raw[4:])
    date = str(int(time.time()))
    msg = tags + "\n"
    msg += echoarea + "\n"
    msg += date + "\n"
    msg += msgfrom + "\n"
    msg += addr + "\n"
    msg += msgto + "\n"
    msg += subject + "\n\n"
    msg += body
    if sys.getsizeof(msg) <= 65535:
        h = hsh(msg)
        save_message(echoarea, h, msg)
        return "msg ok: %s" % h
    else:
        return "error: msg big!"

def debundle(echoarea, bundle):
    bundle = bundle.split()
    for row in bundle:
        vals = row.split(":")
        message = base64.urlsafe_b64decode(vals[1]).decode("utf-8")
        save_message(echoarea, vals[0], message)
