#!/usr/bin/env python3

import base64, hashlib

def is_point(authstr):
    points = open("points.txt").read().split("\n")
    for point in points:
        line = point.split(":")
        if len(line) == 3:
            if line[2] == authstr:
                return line[0], line[1]
    return False, False

def is_pusher(authstr):
    pushers = open("pushers.txt").read().split("\n")
    return authstr in pushers

def hsh(str):
    out = base64.urlsafe_b64encode(hashlib.sha256(str).digest()).decode("utf-8")
    return out.replace('-', '').replace('_', '')[:8].ljust(8,'A')

def save_point(user, hsh):
    addrs = []
    m = 0
    for point in open("points.txt", "r").read().split("\n"):
        if len(point) > 0:
            m += 1
            row = point.split(":")
            addrs.append(int(row[3]))
    for i in range(1, m + 2):
        if not i in addrs:
            point = i
            break
    open("points.txt", "a").write("%s:%s:%s\n" % (hsh, user, point))

def make_point(user):
    hs = hsh(user.encode("utf-8"))
    return hs

if __name__ == "__main__":
    import sys, os
    if not os.path.exists("points.txt"):
        open("points.txt", "w").write("")
    args = sys.argv[1:]
    if len(args) == 1:
        user = args[0]
        hsh = make_point(user)
        save_point(user, hsh)
        print("Username: %s" % user)
        print("Authstr:  %s" % hsh)
    else:
        print("Usage: points.py username")
